package edu.hawaii.ics211;
/**
 * Test class for calculator.java
 * @author Keenan Kinimaka
 * @date 4/4/2016
 */
public class CalculatorTester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/**
		 * Initialize calculator.
		 */
		Calculator c = new Calculator();
		/**
		 * Initialize test variables.
		 */
		int x = 2;
		int y = 1;
		/**
		 * Addition test.  Should produce 3.
		 */
		System.out.println("Addition test: " + c.add(x, y));
		/**
		 * Subtraction test.  Should produce 1.
		 */
		System.out.println("Subtraction test: " + c.subtract(x, y));
		/**
		 * Multiplication test.  Should produce 2.
		 */
		System.out.println("Multiplication test: " + c.multiply(x, y));
		/**
		 * Division test.  Should produce 2.
		 */
		System.out.println("Division test: " + c.divide(x, y));
		/**
		 * Modulo test.  Should produce 0.
		 */
		System.out.println("Modulo test: " + c.modulo(x, y));
		/**
		 * Power test.  Should produce 2.
		 */
		System.out.println("Power test: " + c.pow(x, y));
		/**
		 * Divide test for 0 denominator. Should throw an Arithmatic Exception.
		 */
		try {
			c.divide(1,0);
		}
		catch (ArithmeticException e) {
			System.err.println("Caught ArithmeticException: " + e.getMessage());
		}
		/**
		 * Modulo test for 0 denominator. Should throw an Arithmatic Exception.
		 */
		try {
			c.modulo(1,0);
		}
		catch (ArithmeticException e) {
			System.err.println("Caught ArithmeticException: " + e.getMessage());
		}
	}

}
